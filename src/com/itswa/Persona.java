package com.itswa;

public class Persona {
    private static final int DEBAJO_PESO = -1;
    private static final int PESO_IDEAL = 0;
    private static final int SOBRE_PESO = 1;
    private static final int MAYOR_EDAD = 18;

    private String nombre;
    private int edad;
    private String DNI;
    private char sexo;// H or M
    private Double peso; //Kg
    private Double altura;

    public Persona() {
    }

    public Persona(String nombre, int edad, char sexo) {
        this(nombre,edad,sexo,0.0,0.0);
    }

    public Persona(String nombre, int edad, char sexo, Double peso, Double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.peso = peso;
        this.altura = altura;
        comprobarSexo(sexo);
        generaDNI();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setedad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public int calcularIMC(){
        Double IMC = peso/altura*altura;
        if(IMC<20)
            return DEBAJO_PESO;
        else if (IMC>25)
            return PESO_IDEAL;
        else
            return SOBRE_PESO;
    }

    public Boolean esMayorDeEdad(){
        if(edad>= MAYOR_EDAD)
            return true;
        else
            return false;
    }

    public void comprobarSexo(char sexo){
        if(sexo != 'H' && sexo!='M')
            this.sexo = 'H';
        else
            this.sexo = sexo;
    }

    private void generaDNI(){
        int cadenaDigitos = (int)(Math.random() * (100000000));
        char[] castAcssi = Character.toChars((cadenaDigitos%25)+65);
        DNI = Integer.toString(cadenaDigitos) + castAcssi[0];
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", DNI='" + DNI + '\'' +
                ", sexo='" + sexo + '\'' +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }


}
