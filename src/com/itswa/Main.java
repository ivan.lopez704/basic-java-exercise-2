package com.itswa;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Persona persona = new Persona("Ivan Lopez",34,'H',104.0,1.75);
        System.out.println(persona.toString());
        if(persona.esMayorDeEdad())
            System.out.println("La persona es mayor de edad");
        else
            System.out.println("La persona es menor de edad");

    }
}
